freeStyleJob('abc-job') {

    scm {
         git {
            branch('master')
            remote {
                credentials('github-ci-key')
              	url('https://gitlab.com/shivamt31/public-temp-repo')
            }
    	}
    }
    steps {
      dsl {
            external('abc.groovy','xyz.groovy')
            removeAction('DISABLE')
            ignoreExisting()
            additionalClasspath('lib')
        }
 
    }

}